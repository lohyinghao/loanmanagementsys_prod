package com.fdmgroup.LoanManagementSystem.calculator;

import java.util.HashMap;
import java.util.Map;

import com.fdmgroup.LoanManagementSystem.entity.Loan;

// Interest Rate Calculator for 2018 Q4
// implements the following formula for tabulating interest rate
// a*(loanAmount/annualIncome)^(0.5) + b*(credit rating coefficient) + c*(terms/6)
// If the calculated interest rate is less than 7%, interest rate will be set at 7%
public class InterestRateCalculator_2018_Q4 implements InterestRatesCalculaterAble {
	
	private static final double a = 0.1133; //ratio of loan to annual income
	private static final double b = 0.007197; //ratio of credit rating
	private static final double c = 0.008028; //ratio of term
    private static final Map<Character, Integer> creditRatingMap;
    static
    {
    	//credit rating coefficient
    	creditRatingMap = new HashMap<Character, Integer>();
    	creditRatingMap.put('A',1);
    	creditRatingMap.put('B',2);
    	creditRatingMap.put('C',3);
    	creditRatingMap.put('D',4);
    	creditRatingMap.put('E',5);
    	creditRatingMap.put('F',6);
    	creditRatingMap.put('G',7);
    }
	
	public double calculate(Loan loan) {
		double loanAmount = loan.getLoanAmount();
		double annualIncome = loan.getAnnualIncome();
		double numericCreditRating = creditRatingMap.get(loan.getCreditRating());
		double termsInSixMonthsBlock = loan.getTermsInMonths()/6;
		
		double interestRate_1stTerm = a * Math.sqrt(loanAmount/annualIncome);
		double interestRate_2ndTerm = b * numericCreditRating;
		double interestRate_3rdTerm = c * termsInSixMonthsBlock;
		double interestRate = interestRate_1stTerm + interestRate_2ndTerm + interestRate_3rdTerm;
		
		//rounding results to 4 decimal points.
		double roundedOffinterestRate = (double) Math.round(interestRate * 10000) / 10000;
		
		return Math.max(roundedOffinterestRate,0.07);
		
	}

	
}
