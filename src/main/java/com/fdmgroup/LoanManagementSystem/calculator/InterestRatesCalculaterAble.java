package com.fdmgroup.LoanManagementSystem.calculator;

import com.fdmgroup.LoanManagementSystem.entity.Loan;

// This interface ensure future iterations of the 
// interestRateCalculator class will be able to 
// work with the loanService.
public interface InterestRatesCalculaterAble {

	double calculate(Loan loan);

}
