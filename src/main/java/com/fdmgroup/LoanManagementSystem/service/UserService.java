package com.fdmgroup.LoanManagementSystem.service;

import java.util.Base64;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fdmgroup.LoanManagementSystem.entity.User;
import com.fdmgroup.LoanManagementSystem.repository.UserRepository;

//Manage business logic regarding user object
@Component
public class UserService {
	
	@Autowired
	private UserRepository userRepository;

	public void add(User user) {
		String encryptedPW = caesarCipherEncrypt(user.getPassword());
		user.setPassword(encryptedPW);
		userRepository.save(user);
	}

	public User authenticateUser(User user) {
		String encryptedPW = caesarCipherEncrypt(user.getPassword());
		List<User> authenticateResult = userRepository.authenticUser(user.getUserName(), encryptedPW);
		if (authenticateResult.size() == 1) {
			authenticateResult.get(0).setPassword("");
			return authenticateResult.get(0);
		} else {
			return null;
		}

	}

	private String caesarCipherEncrypt(String plain) {
		String b64encoded = Base64.getEncoder().encodeToString(plain.getBytes());

		// Reverse the string
		String reverse = new StringBuffer(b64encoded).reverse().toString();

		StringBuilder tmp = new StringBuilder();
		final int OFFSET = 4;
		for (int i = 0; i < reverse.length(); i++) {
			tmp.append((char) (reverse.charAt(i) + OFFSET));
		}
		return tmp.toString();
	}

}
