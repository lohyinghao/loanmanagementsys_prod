package com.fdmgroup.LoanManagementSystem.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fdmgroup.LoanManagementSystem.approval.LoanApprovalAble;
import com.fdmgroup.LoanManagementSystem.calculator.InterestRatesCalculaterAble;
import com.fdmgroup.LoanManagementSystem.entity.Loan;
import com.fdmgroup.LoanManagementSystem.repository.LoanRepository;

//Manage business logic regarding loan object
@Component
public class LoanService {
	
	@Autowired
	private LoanRepository loanRepository;
	
	@Autowired
	private InterestRatesCalculaterAble interestRatesCalculator;

	@Autowired
	private LoanApprovalAble loanApproval;
	
	public void add(Loan loan) {
		loanRepository.save(loan);
	}
	
	public List<Loan> getAll() {
		return loanRepository.findAll();
	}
	
	public Loan getOne(int id) {
		return loanRepository.findOne(id);
	}
	
	public Loan calculateInterestRates(Loan loan){
		loan.setInterestRates(interestRatesCalculator.calculate(loan));
		return loan;
	}
	
	public boolean requestApproval(Loan loan){
		return loanApproval.requestApproval(loan);
	}
}
