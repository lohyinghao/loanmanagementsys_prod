package com.fdmgroup.LoanManagementSystem.approval;

import com.fdmgroup.LoanManagementSystem.entity.Loan;

//This interface ensure future iterations of the 
//LoanApproval class will be able to 
//work with the loanService.
public interface LoanApprovalAble {

	boolean requestApproval(Loan loan);

}