package com.fdmgroup.LoanManagementSystem.approval;

import com.fdmgroup.LoanManagementSystem.entity.Loan;

//Loan Approval for 2018 Q4
//Accept Loan with interest rate below 30%
//Reject Loan with interest rate above 30%
public class LoanApproval_2018_Q4 implements LoanApprovalAble {

	double limitForApprovalRates = 0.3;
	
	@Override
	public boolean requestApproval(Loan loan) {
		if(loan.getInterestRates() < limitForApprovalRates) {
			return true;
		}
		return false;
	}

}
