
package com.fdmgroup.LoanManagementSystem.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.fdmgroup.LoanManagementSystem.entity.Loan;

@Repository
public interface LoanRepository extends JpaRepository<Loan, Integer> {
	
}
