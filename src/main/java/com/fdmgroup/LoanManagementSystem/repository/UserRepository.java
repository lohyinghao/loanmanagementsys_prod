package com.fdmgroup.LoanManagementSystem.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.fdmgroup.LoanManagementSystem.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User,Integer>{
	
    @Query("SELECT p FROM User p WHERE p.userName = ?1 AND p.password = ?2")
    public List<User> authenticUser(String userName, String password);
}
