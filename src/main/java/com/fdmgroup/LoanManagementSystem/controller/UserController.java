package com.fdmgroup.LoanManagementSystem.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fdmgroup.LoanManagementSystem.entity.User;
import com.fdmgroup.LoanManagementSystem.service.UserService;

//Controller for REST api calls relating to user objects
@RestController
@RequestMapping("/login")
public class UserController {

	@Autowired
	private UserService userService;

	//@CrossOrigin //To uncomment in development environment when frontend and backend are run on different server
	@RequestMapping(method=RequestMethod.POST)
	public User login(@RequestBody User user) {
		return userService.authenticateUser(user);
	}
	
	//method to populate DB (Will be removed in actual production build)
	@RequestMapping(value = "/setup", method = RequestMethod.GET)
	public String setup() {
		User user1 = new User("yinghao.loh","password");
		User user2 = new User("dale.eric","qwerty");
		userService.add(user1);
		userService.add(user2);
		return "User table setup completed";
	}
	
}
