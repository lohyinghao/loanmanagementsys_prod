package com.fdmgroup.LoanManagementSystem.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fdmgroup.LoanManagementSystem.approval.LoanApprovalAble;
import com.fdmgroup.LoanManagementSystem.approval.LoanApproval_2018_Q4;
import com.fdmgroup.LoanManagementSystem.calculator.InterestRatesCalculaterAble;
import com.fdmgroup.LoanManagementSystem.entity.Loan;
import com.fdmgroup.LoanManagementSystem.service.LoanService;

//Controller for REST api calls relating to loan objects
@RestController
@RequestMapping("/loan")
public class LoanController {

	@Autowired
	private ApplicationContext context;
	
	@Autowired
	private LoanService loanService;
	
	//method to populate DB (Will be removed in actual production build)
	@RequestMapping(value = "/setup", method = RequestMethod.GET)
	public String setup() throws InterruptedException {
		Loan loan1 = new Loan("alfred","C91G93CJ",7200.00,'C',36, 51738.00,0.1358,"yinghao.loh");
		loanService.add(loan1);
		Thread.sleep(1000);
		Loan loan2 = new Loan("kerry","77YP8NW6",12000.00,'A',36, 240000.00,0.0707,"yinghao.loh");
		loanService.add(loan2);
		Thread.sleep(1000);
		Loan loan3 = new Loan("julia","T5VYG862",3100.00,'B',18, 85000.00,0.0943,"dale.eric");
		loanService.add(loan3);
		Thread.sleep(1000);
		Loan loan4 = new Loan("amanda","FCS7OSQY",3200.00,'A',36, 42000.00,0.0796,"dale.eric");
		loanService.add(loan4);
		Thread.sleep(1000);
		Loan loan5 = new Loan("jack","0APQX40K",11000.00,'A',36, 100000.00,0.0734,"dale.eric");
		loanService.add(loan5);
		return "updated";
	}

	//@CrossOrigin //To uncomment in development environment when frontend and backend are run on different server
	@RequestMapping(value = "/new", method = RequestMethod.POST)
	public Loan newLoan(@RequestBody Loan loan) {
		loanService.add(loan);
		return loan;
	}

	//@CrossOrigin //To uncomment in development environment when frontend and backend are run on different server
	@RequestMapping(value = "/calculate", method = RequestMethod.POST)
	public Loan calculate(@RequestBody Loan loan) {
		return loanService.calculateInterestRates(loan);
	}
	
	//@CrossOrigin //To uncomment in development environment when frontend and backend are run on different server
	@RequestMapping(value = "/approval", method = RequestMethod.POST)
	public boolean requestApproval(@RequestBody Loan loan) {
		return loanService.requestApproval(loan);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Loan getOne(@PathVariable("id") int id) {
		return loanService.getOne(id);
	}
	
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public List<Loan> getAll() {
		return loanService.getAll();
	}

}