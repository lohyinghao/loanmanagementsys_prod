package com.fdmgroup.LoanManagementSystem.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table (name="Loan")
public class Loan implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "loan_SEQ")
    @SequenceGenerator(sequenceName = "loan_seq", allocationSize = 1000, name = "loan_SEQ")
	private int loanID;
	private String customerName;
	private String identificalNumber;
	private double loanAmount;
	private char creditRating;
	private int termsInMonths;
	private double annualIncome;
	private double interestRates;
	private String loanOfficer;
	
	public Loan(String customerName, String identificalNumber, double loanAmount, 
			char creditRating, int termsInMonths,
			double annualIncome, double interestRates, String loanOfficer) {
		super();
		this.customerName = customerName;
		this.identificalNumber = identificalNumber;
		this.loanAmount = loanAmount;
		this.creditRating = creditRating;
		this.termsInMonths = termsInMonths;
		this.annualIncome = annualIncome;
		this.interestRates = interestRates;
		this.loanOfficer = loanOfficer;
	}
	
	public Loan() {
		super();
	}

	public int getLoanID() {
		return loanID;
	}
	public void setLoanID(int loanID) {
		this.loanID = loanID;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getIdentificalNumber() {
		return identificalNumber;
	}
	public void setIdentificalNumber(String identificalNumber) {
		this.identificalNumber = identificalNumber;
	}
	public double getLoanAmount() {
		return loanAmount;
	}
	public void setLoanAmount(double loanAmount) {
		this.loanAmount = loanAmount;
	}
	public char getCreditRating() {
		return creditRating;
	}
	public void setCreditRating(char creditRating) {
		this.creditRating = creditRating;
	}
	public int getTermsInMonths() {
		return termsInMonths;
	}
	public void setTermsInMonths(int termsInMonths) {
		this.termsInMonths = termsInMonths;
	}
	public double getAnnualIncome() {
		return annualIncome;
	}
	public void setAnnualIncome(double annualIncome) {
		this.annualIncome = annualIncome;
	}
	public double getInterestRates() {
		return interestRates;
	}
	public void setInterestRates(double interestRates) {
		this.interestRates = interestRates;
	}

	public String getLoanOfficer() {
		return loanOfficer;
	}

	public void setLoanOfficer(String loanOfficer) {
		this.loanOfficer = loanOfficer;
	}
}
