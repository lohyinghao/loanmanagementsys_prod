package com.fdmgroup.LoanManagementSystem.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table (name="UserRecords")
public class User {

	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_SEQ")
    @SequenceGenerator(sequenceName = "user_seq", allocationSize = 1, name = "user_SEQ")
	private int userID;
	@Column(unique = true)
	private String userName;
	private String password;
	
	public User() {
		super();
	}
	
	public User(String userName, String password) {
		super();
		this.userName = userName;
		this.password = password;
	}
	
	public int getUserID() {
		return userID;
	}
	public void setUserID(int userID) {
		this.userID = userID;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
